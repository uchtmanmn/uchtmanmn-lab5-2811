/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 5: Tourists
 * Author:     Dr. Yoder and Molly Uchtman
 * Date:       13 Jan 2019
 * updated 13 Feb 2019
 */
package lab5tourists;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javafx.application.Application;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import lab5tourists.mobileEntities.Bus;
import lab5tourists.mobileEntities.Car;
import lab5tourists.mobileEntities.MobileEntity;
import lab5tourists.mobileEntities.Person;


/**
 * This application represents a city map which a tourist can explore solving a variety of
 * challenges.
 *
 * The CityMap holds all of the items appearing on the map, populating itself at the beginning of
 * the game.
 *
 * The basic game framework is based on a SO answer about how to make Canvas elements clickable [1],
 * but in the end, we don't use a Canvas.
 *
 * [1] https://stackoverflow.com/questions/27999430/javafx-clickable-line-on-canvas
 */
public class CityMap extends Application {

    public static final int NUM_TOURISTS = 15;

    /** Width of the "challenges" space on the right side of the map in pixels */
    public static final int MIN_CHALLENGES_WIDTH = 250;
    private static Image backgroundImage = new Image(CityMap.class.getResource("img/map.png")
            .toString());
    private Pane overlay = new Pane();

    private ImageView backgroundView = new ImageView(backgroundImage);
    private Pane challengePane;
    private Pane buttonPane;
    private Collection<Taggable> taggables = new ArrayList<>();

    private List<MobileEntity> mobileEntities = new ArrayList<>();
    private List<Museum> museums = new ArrayList<>();

    /** For ease of access, there is a single character accessible as a sort of Singleton. */
    private static MobileEntity mainCharacter = null;

    public static boolean busChallengeActive;
    private static final Text MSOEText = new Text("Challenge: Find all the letters in MSOE\nGoal:MSOE");
    private static Text MSOEProgress;
    private static final Text artText = new Text("\n\nChallenge: Find art");
    private static Text artProgress = new Text("Artistic works found!");
    private static final ImageView woodGathererImageView= new ImageView(new Image("lab5tourists/img/wood-gatherer.png"));
    private static Text busText = new Text("Challenge: Find all the letters in BUS\nGoal:BUS");
    private static Text busProgress;
    public static final WordObserver wordObserver = new WordObserver();
    public static final ArtMuseumObserver artObserver = new ArtMuseumObserver();
    public static final Text busChallengeComplete = new Text("Challenge Complete");
    public static final Text msoeChallengeComplete = new Text("Challenge Complete");
    public static final Text artChallengeComplete = new Text("Challenge Complete");



    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Each taggable must also call addTaggableToMap() to add itself to the map.
     * @param node node the GUI element to add.
     */
    public void addNodeToMap(Node node) {
        overlay.getChildren().addAll(node);
    }

    /**
     * Add a taggable to the map. Muse also call addNodeToMap for each visual element to be
     * placed on the map.
     * @param taggable the taggable item to be added to the map.
     */
    public void addTaggableToMap(Taggable taggable) {
        taggables.add(taggable);
    }

    /**
     * Adds a visual pane for a Challenge to the map.
     * @param node the visual pane to add.
     */
    public void addChallengeNode(Node node) {
        challengePane.getChildren().addAll(node);
    }

    /**
     * @return a copy of list of the mobile entities found on the map. The entities themselves
     * are NOT copies.
     */
    public List<MobileEntity> getMobileEntities() {
        return new ArrayList<>(mobileEntities);
    }

    /**
     * @return a copy of the list of the museums found on the map.
     * The areas themselves are NOT copies.
     */
    public List<Museum> getMuseums() {
        return new ArrayList<>(museums);
    }

    /**
     * Called by an entity each time it moves.
     *
     * Causes the entity to tag any entities that it now touches.
     *
     * @param entity The entity doing the tagging.
     */
    public void taggedBy(MobileEntity entity) {
        for(Taggable taggable: taggables) {
            if(taggable.isTagged(entity.getLocation())) {
                taggable.taggedBy(entity);
            }
        }
    }

    public double getWidth(){
        return backgroundView.getImage().getWidth();
    }

    public double getHeight() {
        return backgroundView.getImage().getHeight();
    }

    /**
     * Set up the basic map layout.
     *
     * See addEntities() for the fun part (MobileEntities, etc.)
     *
     * @param primaryStage The main window.
     */
    @Override
    public void start(Stage primaryStage) {
        busChallengeActive = false;
        MSOEProgress = new Text("Found: ****");
        busProgress = new Text("Found: ***");
        Pane root = new HBox();
        challengePane = new VBox();
        challengePane.setMinWidth(MIN_CHALLENGES_WIDTH);
        buttonPane = new VBox();
        buttonPane.setMinWidth(MIN_CHALLENGES_WIDTH);
        Pane mapPane = new Pane();

        backgroundView.relocate(0,0);
        mapPane.setMaxWidth(backgroundImage.getWidth());
        mapPane.setMaxHeight(backgroundImage.getHeight());

        overlay.getChildren().addAll(backgroundView);
        mapPane.getChildren().addAll(overlay);
        root.getChildren().addAll(mapPane, buttonPane, challengePane);

        primaryStage.setScene(new Scene(root));
        primaryStage.show();

        addEntities();

        createObjects(new ImageView(CityMap.class.getResource("img/car.png").toString()), new Label("Car"), new CarFactory());
        createObjects(new ImageView(CityMap.class.getResource("img/bus.png").toString()), new Label("Bus"), new BusFactory());
        createObjects(new ImageView(CityMap.class.getResource("img/drone.png").toString()), new Label("Drone"), new DroneFactory());
    }

    private void createObjects(ImageView imageView, Label label, Factory factory){
        HBox hBox = new HBox();
        Button button1 = new Button("+1");
        Button button10 = new Button("+10");
        imageView.setPreserveRatio(true);
        imageView.setFitWidth(100);
        imageView.setFitHeight(100);
        button1.setOnAction(actionEvent -> factory.createMobileEntities(this));
        button10.setOnAction(actionEvent -> {
            for (int i = 0; i < 10; i++) {
                factory.createMobileEntities(this);
            }
        });
        hBox.getChildren().addAll(imageView, label, button1, button10);
        buttonPane.getChildren().add(hBox);
    }

    /**
     * Get the main character Singleton
     * @return the main character (the Person)
     */
    public static MobileEntity getMainCharacter() {
        if(mainCharacter == null) {
            throw new RuntimeException("getMainCharacter called before game was initialized!");
        }
        return mainCharacter;
    }

    /**
     * Set up all the Mobile and non-mobile Entities on the map.
     *
     * Also sets up Challenges.
     */
    private void addEntities() {
        // Each entity places itself on the map, so we place the goal first
        // so it will show at the bottom.
        Person.Goal goal = new Person.Goal(this);

//        for(int i = 0; i < NUM_TOURISTS; i++) {
//            mobileEntities.add(new Car(this));
//        }
//
//        mobileEntities.add(new Bus(this));
//        mobileEntities.add(new Bus(this));

        synchronized (CityMap.class) {
            if(mainCharacter == null) {
                mainCharacter = new Person(this, goal);
            } else {
                System.out.println("Warning: initializing map more than once!");
            }
        }

        museums.add(new Museum(this));

        woodGathererImageView.setFitWidth(MIN_CHALLENGES_WIDTH/3);
        woodGathererImageView.setFitHeight(MIN_CHALLENGES_WIDTH/3);
        challengePane.getChildren().addAll(MSOEText, MSOEProgress, msoeChallengeComplete, artText, artProgress, artChallengeComplete, woodGathererImageView, busText, busProgress, busChallengeComplete);

        busText.setVisible(false);
        busProgress.setVisible(false);
        woodGathererImageView.setVisible(false);
        artProgress.setVisible(false);
        msoeChallengeComplete.setVisible(false);
        artChallengeComplete.setVisible(false);
        busChallengeComplete.setVisible(false);
    }

    public boolean getBusChallengeActive(){
        return busChallengeActive;
    }

    public static void activateBusChallenge(){
        busChallengeActive = true;
        busText.setVisible(true);
        busProgress.setVisible(true);
    }

    public static void showArt(){
        artProgress.setVisible(true);
        woodGathererImageView.setVisible(true);
        artChallengeComplete.setVisible(true);
    }

    public static void updateMSOE(char[] update){
        MSOEProgress.setText("Found: " + new String(update));
        if(new String(update).equalsIgnoreCase("msoe")){
            msoeChallengeComplete.setVisible(true);
        }
    }

    public static void updateBus(char[] update){
        busProgress.setText("Found: " + new String(update));
        if(new String(update).equalsIgnoreCase("bus")){
            busChallengeComplete.setVisible(true);
        }
    }

    public static void MSOEcomplete(){
        //msoeChallengeComplete.setVisible(true);
    }

    public static void busComplete(){
        //busChallengeComplete.setVisible(true);
    }
}