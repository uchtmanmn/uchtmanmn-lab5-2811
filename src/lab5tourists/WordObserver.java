/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 5: Tourists
 * Author:     Molly Uchtman
 * Date:       13 Jan 2019
 */
package lab5tourists;

/**
 * Handles both of the word challenges
 */
public class WordObserver implements Observer {
    private char[] msoeHidden;
    private char[] msoe;
    private char[] bus;
    private char[] busHidden;
    private int busFlag;
    private int msoeFlag;

    public WordObserver(){
        msoeHidden = new char[4];
        bus = new char[4];
        busHidden = new char[3];
        for(int i = 0; i < msoeHidden.length; i++){
            msoeHidden[i] = '*';
        }
        for(int i = 0; i < busHidden.length; i++){
            busHidden[i] = '*';
        }
        msoe = new char[4];
        msoe[0] = 'M';
        msoe[1] = 'S';
        msoe[2] = 'O';
        msoe[3] = 'E';
        bus[0] = 'B';
        bus[1] = 'U';
        bus[2] = 'S';
        busFlag = 0;
        msoeFlag = 0;
    }

    public void update(String plateString){
        char[] plate = plateString.toCharArray();
        for(char letter: plate){
            for(int i = 0; i < 4; i++){
                if(letter == msoe[i]){
                    msoeHidden[i] = msoe[i];
                    CityMap.updateMSOE(msoeHidden);
                    msoeFlag += 1;
                }
            }
            for(int i = 0; CityMap.busChallengeActive && i < 3; i++){
                if(letter == bus[i]){
                    busHidden[i] = bus[i];
                    CityMap.updateBus(busHidden);
                }
            }
        }
    }




}
