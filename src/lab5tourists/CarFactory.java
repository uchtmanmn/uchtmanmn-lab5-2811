package lab5tourists;
/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 7
 * Author:     Molly Uchtman
 * Date:       13 Feb 2019
 */
import lab5tourists.mobileEntities.Car;
/**
 * creates a car on the cityMap
 */
public class CarFactory implements Factory {
    @Override
    public void createMobileEntities(CityMap cityMap) {
        new Car(cityMap);
    }
}
