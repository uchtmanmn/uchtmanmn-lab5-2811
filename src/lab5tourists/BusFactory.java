package lab5tourists;
/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 7
 * Author:     Molly Uchtman
 * Date:       13 Feb 2019
 */
import lab5tourists.mobileEntities.Bus;

/**
 * creates a bus on the cityMap
 */
public class BusFactory implements Factory{

    @Override
    public void createMobileEntities(CityMap cityMap) {
        new Bus(cityMap);
    }
}
