/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 7
 * Author:     Molly Uchtman
 * Date:       13 Feb 2019
 */
package lab5tourists;
/**
 * creates a mobile entity on the cityMap
 */
public interface Factory {
    void createMobileEntities(CityMap cityMap);
}
