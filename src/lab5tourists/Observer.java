/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 5: Tourists
 * Author:     Molly Uchtman
 * Date:       13 Jan 2019
 */
package lab5tourists;

/**
 * the basis for the observer classes
 */
public interface Observer {

    void update(String s);

}
