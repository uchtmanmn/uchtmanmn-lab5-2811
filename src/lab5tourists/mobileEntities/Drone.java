/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 5: Tourists
 * Author:     Molly Uchtman
 * Date:       13 Feb 2019
 */
package lab5tourists.mobileEntities;

import javafx.geometry.Point2D;
import javafx.scene.image.Image;
import lab5tourists.CityMap;
/**
 * A randomly-driving drone
 */
public class Drone extends MobileEntity {
    private final static Image droneImage = new Image(CityMap.class.getResource("img/drone.png").toString());
    private Point2D goal;
    /**
     * Places and displays this entity at a random location on the map. Causes this entity to start
     * moving with a random velocity along a grid direction.
     *
     * @param cityMap The map on which this entity will be placed.
     */
    public Drone(CityMap cityMap) {
        super(cityMap, droneImage);
        goal = chooseRandomPoint();
        setName(Integer.toHexString(this.hashCode()).toUpperCase());
    }

    protected void step(){
        Point2D direction = goal.subtract(getLocation());
        if(Math.hypot(direction.getX(), direction.getY()) < Math.hypot(stepSize.getX(), stepSize.getY())){
            goal = chooseRandomPoint();
        } else {
            stepSize = direction.multiply(Math.hypot(stepSize.getX(), stepSize.getY()) / Math.hypot(direction.getX(), direction.getY()));
        }
        super.step();
    }
}
